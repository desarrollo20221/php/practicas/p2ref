<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $radio=2.4;
        $perimetro=2 * pi()* $radio ;
        $area= pi()* pow($radio, 2);
        
        echo "<p>El radio del circulo es $radio</p>";
        echo "<p>El area del circulo es $area</p>";
        echo "<p>El perimetro del circulo es $perimetro</p>";
        ?>
        
        <div>
            El radio del circulo es <?= $radio ?>
        </div>
        <div>
            El area del circulo es <?= $area ?>
        </div>
        <div>
            El perimetro del circulo es <?= $perimetro ?>
        </div>
    </body>
</html>
